
function Loader(url, done) {
	console.log("Loading:", url);
	if (!url) throw "Empty URL";

	let request = new XMLHttpRequest();
	request.open("GET", url, true);
	request.responseType = "text";

	const parseMpd = function(root) {
		console.log('root:', root);
		function singleElementOr(fn) {
			return function(root, name) {
				const elements = root.getElementsByTagName(name);
				if (elements.length != 1) {
					return fn(elements.length, name);
				}
				return elements[0];
			}
		}

		const singleElementOrUndefined = singleElementOr(function() {});

		const singleElementOrThrow = singleElementOr(function(count, name) {
			throw 'Expected 1 element, got ' + count + ', name=' + name;
		});

		console.log("parsing MPD...");

		const mpd = {
			baseUrl: (function(e) {
				if (!e) return;
				const baseUrl = e.textContent.toString();
				if (baseUrl.indexOf('://') != -1) {
					// this is an absolute URL
					return baseUrl;
				}
				const parentUrl = url.slice(0, url.lastIndexOf('/'));
				return parentUrl + '/' + baseUrl;
			})(singleElementOrUndefined(root, 'BaseURL'))
		};
		console.log("baseUrl:", mpd.baseUrl);

		mpd.periods = [...root.getElementsByTagName('Period')].map(function(periodElement) {
			console.log('periodElement:', periodElement);
			return {
				adaptationSets: [...periodElement.getElementsByTagName('AdaptationSet')].map(function(adaptationSetElement) {
					const segmentTemplateElement = singleElementOrThrow(adaptationSetElement, 'SegmentTemplate');

					const segmentTemplate = {
						media: segmentTemplateElement.getAttribute('media'),
						startNumber: parseInt(segmentTemplateElement.getAttribute('startNumber'), 10),
						initialization: segmentTemplateElement.getAttribute('initialization')
					};

					const adaptationSet = {
						segmentTemplate: segmentTemplate,
						mimeType: adaptationSetElement.getAttribute('mimeType'),
						contentType: adaptationSetElement.getAttribute('contentType')
					};

					adaptationSet.representations = [...adaptationSetElement
							.getElementsByTagName('Representation')].map(function(representationElement) {
								const r = {
									id: representationElement.getAttribute('id'),
									mimeType: representationElement.getAttribute('mimeType'),
									codecs: representationElement.getAttribute('codecs'),
									width: representationElement.getAttribute('width'),
									height: representationElement.getAttribute('height'),
									bandwidth: representationElement.getAttribute('bandwidth')
								};
								if (!r.mimeType) r.mimeType = adaptationSet.mimeType;
								return r;
							});

					return adaptationSet;
				})
			};
		});

		console.log('mpd:', JSON.stringify(mpd, null, 2));

		return mpd;
	}

	request.onreadystatechange = function() {
		if (request.readyState != request.DONE) {
			return;
		}

		try {
			console.log('parsing MPD XML...');
			const doc = (new DOMParser()).parseFromString(request.response, 'text/xml', 0);
			console.log('request.response:', request.response);
			if (doc.documentElement.localName === 'parsererror') throw 'XML parser error';
			const mpd = parseMpd(doc.documentElement);
			console.log('parsing MPD XML... OK');
			done.ok(mpd);
		} catch (e) {
			console.error('parsing MPD XML... ERROR:', e);
			done.error(e);
		}
	}

	request.send();

	this.abort = function() {
		if (!request) return;
		request.abort();
		request = null;
	}
}

function ClosePool() {
	this.objects = [];

	this.close = function() {
		this.objects.forEach(function(o) { o.close(); });
	}
}

function addEventListener(pool, item, name, callback, ...args) {
	item.addEventListener(name, callback, ...args);

	pool.objects.push({
		close: function() {
			item.removeEventListener(name, callback, ...args);
		}
	});
}

function Player(mpd, ui) {
	var closePool = new ClosePool();

	function playPause() {
		if (ui.video.paused) {
			ui.video.play();
		} else {
			ui.video.pause();
		}
	}

	ui.video.pause();

	function updatePlayPauseButton() {
		ui.playPauseButton.innerText = ui.video.paused ? 'Play' : 'Pause';
	}

	addEventListener(closePool, ui.video, 'pause', updatePlayPauseButton);
	addEventListener(closePool, ui.video, 'playing', updatePlayPauseButton);

	addEventListener(closePool, ui.playPauseButton, 'click', playPause);
	addEventListener(closePool, ui.video, 'click', playPause);

	// play first period by default
	const period = mpd.periods[0];
	console.log('period:', period);

	const adaptationSet = (function() {
		let video, audio;

		for (let a of period.adaptationSets) {
			if (a.contentType === 'video') {
				video = a;
			} else if (a.contentType === 'audio') {
				audio = a;
			}
		}

		return {
			video: video,
			audio: audio
		};
	})();

	const representationsList = adaptationSet.video.representations.map(function(r) {
		return r.id;
	});

	this.representations = function() {
		return representationsList;
	}

	function mediaUrl(mpd, template, id, number) {
		let url = mpd.baseUrl + template.replace(/\$RepresentationID\$/g, id);
		if (number != undefined) {
			url = url.replace(/\$Number\$/g, number);
		}
		return url;
	}

	var playback = null;

	this.selectRepresentation = function(id) {
		console.log('playing representation:', id);

		if (playback) {
			console.log('closing previous representation:', playback.representationId);
			playback.closePool.close();
			playback = null;
		}

		playback = {
			closePool: new ClosePool(),
			representationId: id,
			videoSourceBuffer: null,
			index: adaptationSet.video.segmentTemplate.startNumber
		};

		const representation = (function() {
			for (let r of adaptationSet.video.representations) {
				if (r.id === id) return r;
			}
		})();

		ui.video.width = representation.width;
		ui.video.height = representation.height;

		playback.mediaSource = new window.MediaSource();

		addEventListener(playback.closePool, playback.mediaSource, 'sourceended', function() {
			console.log('media sourceended');
		});

		addEventListener(playback.closePool, playback.mediaSource, 'updateend', function() {
			console.log('media updateend');
		});

		ui.video.src = URL.createObjectURL(playback.mediaSource);

		addEventListener(playback.closePool, playback.mediaSource, 'sourceopen', function(e) {
			try {
				const mimeType = adaptationSet.video.mimeType + "; codecs=" + representation.codecs;
				console.log('mimeType:', mimeType);
				playback.videoSourceBuffer = playback.mediaSource.addSourceBuffer(mimeType);

				addEventListener(playback.closePool, playback.videoSourceBuffer, 'updateend', function() {
					console.log('updateend');
				});

				addEventListener(playback.closePool, playback.videoSourceBuffer, 'sourceended', function() {
					console.log('sourceended');
				});

				addEventListener(playback.closePool, playback.videoSourceBuffer, 'error', function() {
					console.log('error');
				});

				loadInitializationSegment();
			} catch (e) {
				console.error('Initialization error:', e);
			}
		});

		function loadInitializationSegment() {
			const url = mediaUrl(mpd, adaptationSet.video.segmentTemplate.initialization, id);
			console.log('initialization url:', url);

			const request = new XMLHttpRequest();
			request.open('GET', url);
			request.send();
			request.responseType = 'arraybuffer';
			request.addEventListener("readystatechange", function() {
				if (request.readyState != request.DONE) return;

				console.log('got initialization data:', request.response);
				try {
					playback.videoSourceBuffer.appendBuffer(new Uint8Array(request.response));
				} catch (e) {
					console.error('Error appending initialization buffer:', e);
				}

				playback.videoSourceBuffer.addEventListener("update", function update() {
					playback.videoSourceBuffer.removeEventListener("update", update);
					loadNextSegment();
				}, false);
			}, false);
		}

		function checkTime() {
			console.log('checking time, current:', ui.video.currentTime, 'duration:', ui.video.duration);
		}

		function loadNextSegment() {
			const url = mediaUrl(mpd, adaptationSet.video.segmentTemplate.media,
					playback.representationId, playback.index);
			console.log('segment url:', url);

			const request = new XMLHttpRequest();
			request.open('GET', url);
			request.send();
			request.responseType = 'arraybuffer';

			request.addEventListener("readystatechange", function() {
				if (request.readyState != request.DONE) return;

				console.log('got segment data, url:', url, 'data:', request.response);
				try {
					playback.videoSourceBuffer.appendBuffer(new Uint8Array(request.response));

					playback.index++;
					loadNextSegment();
				} catch (e) {
					console.error('Error appending segment buffer:', e);
				}
			}, false);

			addEventListener(playback.closePool, ui.video, 'timeupdate', checkTime);

			addEventListener(playback.closePool, ui.video, 'ended', function() {
				ui.video.removeEventListener('timeupdate', checkTime);
			});
		}
	}

	this.close = function() {
		ui.video.pause();
		closePool.close();
	}
}

(function() {
	var loader = null;
	var player = null;

	var ui = {
		loadButton: document.getElementById('loadButton'),
		video: document.getElementById('video'),
		playPauseButton: document.getElementById('playPauseButton'),
		representationsSpan: document.getElementById('representations')
	};

	function load(url) {
		if (player) {
			player.close();
			player = null;
		} else if (loader) {
			loader.abort();
			loader = null;
		}

		var done = {
			ok: function(mpd) {
				loader = null;
				console.log('ok, mpd:', mpd);

				player = new Player(mpd, ui);
				console.log('player.representations:', player.representations());

				function buttonIdForRepresentation(r) {
					return 'representationButton_' + r;
				}

				let representationsData = '<ul>';
				player.representations().forEach(function(r) {
					const buttonId = buttonIdForRepresentation(r);
					representationsData += '<button id="' + buttonId + '">' + r + '</button>';
				});
				representationsData += '</ul>';
				ui.representationsSpan.innerHTML = representationsData;

				player.representations().forEach(function(r) {
					const button = document.getElementById(buttonIdForRepresentation(r));
					button.addEventListener('click', function() {
						console.log('selected representation:', r);
						player.selectRepresentation(r);
					});
				});
			},

			error: function(...e) {
				loader = null;
				console.error('error:', ...e);
			}
		};

		loader = new Loader(url, done);
	};

	ui.loadButton.addEventListener('click', function() {
		var mpdUrl = document.getElementById('mpdUrl').value;
		console.log('mpdUrl:', mpdUrl);
		load(mpdUrl);
	}, false);
})();
